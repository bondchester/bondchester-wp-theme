<?php
/**
 * Template Name: Full Width
 *
 * @package Bondchester
 */

get_header(); ?>

	<main id="primary" class="site-main">
		<div class="container">
			<div class="row">
				<div class="col-md-12 py-5">
					<?php
					while ( have_posts() ) : the_post();

						?>
						<header class="page-title">
							<?php the_title( '<h1 class="entry-title mt-0">', '</h1>' ); ?>
						</header><!-- .entry-header -->

						<div class="page-content">
							<?php
								the_content();

								wp_link_pages( array(
									'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'bondchester' ),
									'after'  => '</div>',
								) );
							?>
						</div><!-- .entry-content -->
						<?php

					endwhile; // End of the loop.
					?>
				</div>
			</div>
		</div>
	</main><!-- #primary -->

<?php
get_footer();
