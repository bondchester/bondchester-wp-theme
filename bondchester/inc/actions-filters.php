<?php

function bondchester_menu_classes($classes, $item, $args) {
	if( $args->theme_location == 'menu-1' ) {
		$classes[] = 'nav-item';
	}
	return $classes;
}
add_filter( 'nav_menu_css_class', 'bondchester_menu_classes', 1, 3 );

function bondchester_add_menuclass($ulclass) {
	return preg_replace( '/<a /', '<a class="nav-link"', $ulclass );
}
add_filter( 'wp_nav_menu', 'bondchester_add_menuclass' );

function bondchester_before_footer_top() {
	?>
	<?php if ( is_active_sidebar( 'top-before-widget-left' ) || is_active_sidebar( 'top-before-widget-center' ) || is_active_sidebar( 'top-before-widget-right' ) ) : ?>
		<section class="bc-section bc-before-widgets" id="<?php echo uniqid( 'bc-before-widgets-' ); ?>">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<?php dynamic_sidebar( 'top-before-widget-left' ); ?>
					</div>
					<div class="col-md-4">
						<?php dynamic_sidebar( 'top-before-widget-center' ); ?>
					</div>
					<div class="col-md-4">
						<?php dynamic_sidebar( 'top-before-widget-right' ); ?>
					</div>
				</div>
			</div>
		</section>
	<?php endif; ?>
	<?php
}
add_action( 'bc_before_footer_top', 'bondchester_before_footer_top' );

function bondchester_before_footer_bottom() {
	?>
	<?php if ( is_active_sidebar( 'bottom-before-widget-left' ) && is_active_sidebar( 'bottom-before-widget-right' ) ) : ?>
		<section class="bg-primary bc-before-widgets" id="<?php echo uniqid( 'bc-before-widgets-' ); ?>">
			<div class="container">
				<div class="row align-items-center text-center">
					<div class="col-md-6">
						<?php dynamic_sidebar( 'bottom-before-widget-left' ); ?>
					</div>
					<div class="col-md-6 p-5 text-center text-white bg-black testimonials">
						<?php dynamic_sidebar( 'bottom-before-widget-right' ); ?>
					</div>
				</div>
			</div>
		</section>
	<?php endif; ?>
	<?php
}
add_action( 'bc_before_footer_bottom', 'bondchester_before_footer_bottom' );