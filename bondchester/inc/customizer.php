<?php
/**
 * bondchester Theme Customizer
 *
 * @package Bondchester
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function bondchester_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector'        => '.site-title a',
			'render_callback' => 'bondchester_customize_partial_blogname',
		) );
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector'        => '.site-description',
			'render_callback' => 'bondchester_customize_partial_blogdescription',
		) );
	}

	/**
	 * Add our Sample Section
	 */
	$wp_customize->add_section( 'advanced',
		array(
			'title' => __( 'Advanced' ),
			'priority' => 30, // Not typically needed. Default is 160
			'capability' => 'edit_theme_options',
		)
	);

	$wp_customize->add_setting( 'google_analytics',
		array(
			'default' => '', // Optional.
			'transport' => 'refresh', // Optional. 'refresh' or 'postMessage'. Default: 'refresh'
			'type' => 'theme_mod', // Optional. 'theme_mod' or 'option'. Default: 'theme_mod'
			'capability' => 'edit_theme_options', // Optional. Default: 'edit_theme_options'
		)
	);

	$wp_customize->add_setting( 'fb_pixel_id',
		array(
			'default' => '', // Optional.
			'transport' => 'refresh', // Optional. 'refresh' or 'postMessage'. Default: 'refresh'
			'type' => 'theme_mod', // Optional. 'theme_mod' or 'option'. Default: 'theme_mod'
			'capability' => 'edit_theme_options', // Optional. Default: 'edit_theme_options'
		)
	);

	$wp_customize->add_control(
		'google_analytics_code', 
		array(
			'label'    => __( 'GA Tracking ID', 'mytheme' ),
			'section'  => 'advanced',
			'settings' => 'google_analytics',
			'type'     => 'text',
			'description' => 'Google Analytics enables you to track  the visitors to your store, and generates reports that will help you with your marketing. <a href="#">Learn more about Google Analytics.<a>',
			'input_attrs' => array( 'placeholder' => __( 'Enter your GA Tracking ID' ) )
		)
	);

	$wp_customize->add_control(
		'facebook_pixel_id', 
		array(
			'label'    => __( 'Facebook Pixel', 'mytheme' ),
			'section'  => 'advanced',
			'settings' => 'fb_pixel_id',
			'type'     => 'text',
			'description' => 'Facebook pixel helps you create ad compaign to find new customers that look most like your buyers. <a href="#">Learn more about Facebook Pixel.<a>',
			'input_attrs' => array( 'placeholder' => __( 'Pase your Facebook Pixel ID here' ) )
		)
	);
}
add_action( 'customize_register', 'bondchester_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function bondchester_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function bondchester_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function bondchester_customize_preview_js() {
	wp_enqueue_script( 'bondchester-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'bondchester_customize_preview_js' );

function bondchester_code_js() {
	?>
	<?php if ( get_theme_mod( 'google_analytics' ) != '' ) { ?>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());

			gtag('config', '<?php echo get_theme_mod( 'google_analytics' ); ?>');
		</script>
	<?php } ?>
	
	<?php if ( get_theme_mod( 'fb_pixel_id' ) != '' ) { ?>
		<!-- Facebook Pixel Code -->
		<script>
			!function(f,b,e,v,n,t,s)
			{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
			n.callMethod.apply(n,arguments):n.queue.push(arguments)};
			if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
			n.queue=[];t=b.createElement(e);t.async=!0;
			t.src=v;s=b.getElementsByTagName(e)[0];
			s.parentNode.insertBefore(t,s)}(window, document,'script',
			'https://connect.facebook.net/en_US/fbevents.js');
			fbq('init', '<?php echo get_theme_mod( 'fb_pixel_id' ); ?>');
			fbq('track', 'PageView');
		</script>
		<noscript>
			<img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=your-pixel-id-goes-here&ev=PageView&noscript=1"/>
		</noscript>
		<!-- End Facebook Pixel Code -->
	<?php } ?>
	<?php
}
add_action( 'wp_head', 'bondchester_code_js' );
