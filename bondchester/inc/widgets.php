<?php

// Exit if access directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Bondchester_Widgets' ) ) {
	class Bondchester_Widgets {
		public function __construct() {
			add_action( 'widgets_init', array( $this, 'register_sidebar' ) );
		}

		public function register_sidebar() {

			$defaults = array(
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h2 class="widgettitle">',
				'after_title'   => '</h2>',
			);

			// register_sidebar( array_merge( $defaults, array(
			// 	'name' => esc_html__( 'Header Left', 'bondchester' ),
			// 	'id' => 'header-left',
			// 	'description' => __( 'Widgets in this area will be shown on header left.', 'bondchester' ),
			// ) ) );

			register_sidebar( array_merge( $defaults, array(
				'name' => esc_html__( 'Header Right', 'bondchester' ),
				'id' => 'header-right',
				'description' => __( 'Widgets in this area will be shown on header right.', 'bondchester' ),
			) ) );

			for ($i=1; $i <= 4; $i++) { 
				register_sidebar( array_merge( $defaults, array(
					'name' => esc_html__( 'Footer Widget ' . $i, 'bondchester' ),
					'id' => 'footer-widget-' . $i,
					'description' => __( 'Widgets in this area will be shown on footer.', 'bondchester' ),
				) ) );
			}

			register_sidebar( array_merge( $defaults, array(
				'name' => esc_html__( 'Page Sidebar', 'bondchester' ),
				'id' => 'page-sidebar',
				'description' => __( 'Widgets in this area will be shown on all pages.', 'bondchester' ),
			) ) );

			register_sidebar( array_merge( $defaults, array(
				'name' => esc_html__( 'Post Sidebar', 'bondchester' ),
				'id' => 'post-sidebar',
				'description' => __( 'Widgets in this area will be shown on all single post.', 'bondchester' ),
			) ) );

			register_sidebar( array_merge( $defaults, array(
				'name' => esc_html__( 'Top Before Widget Left', 'bondchester' ),
				'id' => 'top-before-widget-left',
				'description' => __( 'Widgets in this area will be shown before widget footer area.', 'bondchester' ),
			) ) );

			register_sidebar( array_merge( $defaults, array(
				'name' => esc_html__( 'Top Before Widget Center', 'bondchester' ),
				'id' => 'top-before-widget-center',
				'description' => __( 'Widgets in this area will be shown before widget footer area.', 'bondchester' ),
			) ) );

			register_sidebar( array_merge( $defaults, array(
				'name' => esc_html__( 'Map', 'bondchester' ),
				'id' => 'map-widget',
			) ) );

			register_sidebar( array_merge( $defaults, array(
				'name' => esc_html__( 'Bottom Before Widget Left', 'bondchester' ),
				'id' => 'bottom-before-widget-left',
				'description' => __( 'Widgets in this area will be shown before widget footer area.', 'bondchester' ),
			) ) );

			register_sidebar( array_merge( $defaults, array(
				'name' => esc_html__( 'Bottom Before Widget Right', 'bondchester' ),
				'id' => 'bottom-before-widget-right',
				'description' => __( 'Widgets in this area will be shown before widget footer area.', 'bondchester' ),
			) ) );

		}
	}
}

new Bondchester_Widgets();