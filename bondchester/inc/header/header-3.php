<header class="site-header">
    <div id="top-header" class="tt-fixed-header bg-secondary">
        <div class="container-fluid clearfix">
            <div id="tt-info" class="text-right px-4">
                <a href="tel:+61-08-9350-5173">
					<span id="tt-info-phone"><i class="fas fa-phone-square"></i> Call Us Now +61 08 9350 5173</span>
				</a>
				<a href="mailto:KellyC@transitainerwa.com.au">
					<span id="tt-info-email"><i class="fas fa-envelope"></i> KellyC@transitainerwa.com.au</span>
				</a>
                <?php echo do_shortcode('[creative-social-icons facebook="https://www.facebook.com/Transitainer" instagram="https://www.instagram.com/transitainer_wa/"]'); ?>
			</div>
			
        </div>
    </div>
    <nav class="navbar navbar-expand-lg navbar-light xxfixed-top">
        <div class="container-fluid">
            <a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>">
                <?php
				if (has_custom_logo()) {
					echo wp_get_attachment_image(get_theme_mod('custom_logo'), 'full');
				} else {
					bloginfo('name');
				}
				?>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".navbar-collapse" aria-expanded="true" aria-label="Toggle navigation">
                <svg class="icon" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M3 17C3 17.5523 3.44772 18 4 18H20C20.5523 18 21 17.5523 21 17V17C21 16.4477 20.5523 16 20 16H4C3.44772 16 3 16.4477 3 17V17ZM3 12C3 12.5523 3.44772 13 4 13H20C20.5523 13 21 12.5523 21 12V12C21 11.4477 20.5523 11 20 11H4C3.44772 11 3 11.4477 3 12V12ZM4 6C3.44772 6 3 6.44772 3 7V7C3 7.55228 3.44772 8 4 8H20C20.5523 8 21 7.55228 21 7V7C21 6.44772 20.5523 6 20 6H4Z" fill="#212529"></path>
                </svg>
            </button>
            <?php
			wp_nav_menu(array(
				'theme_location' => 'menu-1',
				'container' => 'div', // no need to wrap `wp_nav_menu` manually
				'container_class' => 'collapse navbar-collapse',
				// 'container_id' => 'collapse-1',
				'menu_class' => 'nav navbar-nav ml-auto',
				'depth' => 2,
				'walker' => new WP_Bootstrap_Navwalker()
			));
			?>
        </div>
    </nav>
</header> 