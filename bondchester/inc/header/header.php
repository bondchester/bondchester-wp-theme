<header id="masthead" class="site-header">
	<div class="container-fluid">
		<nav class="navbar navbar-light">
			<div class="site-branding">
				<?php
				if ( has_custom_logo() ) {
					the_custom_logo();
				} else {
					if ( is_front_page() && is_home() ) : ?>
						<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
					<?php else : ?>
						<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
					<?php
					endif;
					$description = get_bloginfo( 'description', 'display' );
					if ( $description || is_customize_preview() ) : ?>
						<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
					<?php
					endif; 
				}
				?>
			</div><!-- .site-branding -->
			<?php
			if ( is_active_sidebar( 'header-right' ) ) {
				dynamic_sidebar( 'header-right' );
			}
			?>
		</nav>
	</div>

	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<div class="container-fluid">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNavDropdown">
				<?php
					wp_nav_menu( array(
						'theme_location' => 'menu-1',
						'container' => 'div', // no need to wrap `wp_nav_menu` manually
						'container_class' => 'collapse navbar-collapse',
						// 'container_id' => 'collapse-1',
						'menu_class' => 'nav navbar-nav ml-auto',
						'depth' => 2,
						'walker' => new WP_Bootstrap_Navwalker()
					) );
				?>
			</div>
		</div>
	</nav>
</header><!-- #masthead -->