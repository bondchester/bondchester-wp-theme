<header id="masthead" class="site-header">
	<nav class="navbar navbar-expand-lg navbar-light">
		<div class="container">

			<!-- Navbar: Brand -->
			<a class="navbar-brand d-lg-none" href="<?php echo esc_url( home_url( '/' ) ); ?>">
				<?php
				if ( has_custom_logo() ) {
					echo wp_get_attachment_image( get_theme_mod( 'custom_logo' ), 'full' );
				} else {
					bloginfo( 'name' );
				}
				?>
			</a>

			<!-- Navbar: Toggler -->
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			
			<!-- Navbar: Collapse -->
			<div class="collapse navbar-collapse" id="navbarSupportedContent">

				<!-- Navbar navigation: Left -->
				<?php
					wp_nav_menu( array(
						'theme_location' => 'menu-1',
						'container' => 'div', // no need to wrap `wp_nav_menu` manually
						'container_class' => 'collapse navbar-collapse',
						// 'container_id' => 'collapse-1',
						'menu_class' => 'nav navbar-nav ml-auto',
						'depth' => 2,
						'walker' => new WP_Bootstrap_Navwalker()
					) );
				?>

				<!-- Navbar: Brand -->
				<a class="navbar-brand d-none d-lg-flex" href="<?php echo esc_url( home_url( '/' ) ); ?>">
					<?php
					if ( has_custom_logo() ) {
						echo wp_get_attachment_image( get_theme_mod( 'custom_logo' ), 'full' );
					} else {
						bloginfo( 'name' );
					}
					?>
				</a>

				<!-- Navbar navigation: Right -->
				<div class="navbar-nav ml-auto nav-right">
					<?php
					if ( is_active_sidebar( 'header-right' ) ) {
						dynamic_sidebar( 'header-right' );
					}
					?>
				</div>

			</div> <!-- / .navbar-collapse -->

		</div> <!-- / .container -->
	</nav>
</header>