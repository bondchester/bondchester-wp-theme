<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Bondchester
 */

?>
<?php echo 'ds' . get_theme_mod( 'google_analytics_code' ); ?>
<?php do_action( 'bc_before_footer_top' ); ?>

<?php do_action( 'bc_before_footer_bottom' ); ?>

<footer id="colophon" class="site-footer" data-style="dark">
	<div class="footer-widgets">
		<div class="container">
			<div class="row">
				<?php for ( $i=0; $i <= 4 ; $i++ ) { ?>
					<?php if ( is_active_sidebar( 'footer-widget-' . $i ) ) { ?>
						<div class="col-lg col-sm-6 col-xs-12">
							<?php dynamic_sidebar( 'footer-widget-' . $i ); ?>
						</div>
					<?php } ?>
				<?php } ?>
			</div>
		</div>
	</div>
	<div class="site-info">
		<div class="container">
			<div class="row">
				<div class="col-md-6 text-center text-sm-left">
					<a href="<?php echo esc_url( __( 'https://wordpress.org/', '_s' ) ); ?>"><?php
						/* translators: %s: CMS name, i.e. WordPress. */
						printf( esc_html__( 'Copyright © 2019 %s. All Rights Reversed.', 'bondchester' ), get_bloginfo( 'name' ) );
					?></a>
				</div>
				<div class="col-md-6 text-center text-sm-right">
					<a href="<?php echo esc_url( __( 'https://wordpress.org/', '_s' ) ); ?>"><?php
						/* translators: %s: CMS name, i.e. WordPress. */
						printf( __( 'site by <a href="%2$s">%1$s</a>', 'bondchester' ), 'creative imagery', esc_url( 'https://creativeimagery.com.au/' ) );
					?></a>
				</div>
			</div>
		</div>
	</div><!-- .site-info -->
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
