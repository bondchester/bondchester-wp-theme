<?php
/**
 * Bondchester functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Bondchester
 */

if ( ! function_exists( 'bondchester_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function bondchester_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on bondchester, use a find and replace
		 * to change 'bondchester' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'bondchester', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'bondchester' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( '_s_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );

		// Adding support for core block visual styles.
		add_theme_support( 'wp-block-styles' );

		// Add support for full and wide align images.
		add_theme_support( 'align-wide' );

		// Add support for custom color scheme.
		add_theme_support( 'editor-color-palette', array(
			array(
				'name'  => __( 'Strong Blue', 'bondchester' ),
				'slug'  => 'strong-blue',
				'color' => '#0073aa',
			),
			array(
				'name'  => __( 'Lighter Blue', 'bondchester' ),
				'slug'  => 'lighter-blue',
				'color' => '#229fd8',
			),
			array(
				'name'  => __( 'Very Light Gray', 'bondchester' ),
				'slug'  => 'very-light-gray',
				'color' => '#eee',
			),
			array(
				'name'  => __( 'Very Dark Gray', 'bondchester' ),
				'slug'  => 'very-dark-gray',
				'color' => '#444',
			),
		) );

		// Add support for responsive embeds.
		add_theme_support( 'responsive-embeds' );
	}
endif;
add_action( 'after_setup_theme', 'bondchester_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function bondchester_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'bondchester_content_width', 640 );
}
add_action( 'after_setup_theme', 'bondchester_content_width', 0 );

/**
 * Register Google Fonts
 */
function bondchester_fonts_url() {
	$fonts_url = '';

	/*
	 *Translators: If there are characters in your language that are not
	 * supported by Open Sans, translate this to 'off'. Do not translate
	 * into your own language.
	 */
	$notoserif = esc_html_x( 'on', 'Open Sans font: on or off', 'bondchester' );

	if ( 'off' !== $notoserif ) {
		$font_families = array();
		$font_families[] = 'Open Sans:300,300i,400,400i,600,600i,700,700i,800,800i';

		$query_args = array(
			'family' => urlencode( implode( '|', $font_families ) ),
			'subset' => urlencode( 'latin,latin-ext' ),
		);

		$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
	}

	return $fonts_url;

}

/**
 * Enqueue scripts and styles.
 */
function bondchester_scripts() {

	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css' );

	wp_enqueue_style( 'font-awesome-5', get_template_directory_uri() . '/assets/css/font-awesome/fontawesome.min.css' );
	wp_enqueue_style( 'font-awesome-brands', get_template_directory_uri() . '/assets/css/font-awesome/brands.min.css' );
	wp_enqueue_style( 'font-awesome-solid', get_template_directory_uri() . '/assets/css/font-awesome/solid.min.css' );
	wp_enqueue_style( 'font-awesome-regular', get_template_directory_uri() . '/assets/css/font-awesome/regular.min.css' );
	
	wp_enqueue_style( 'bondchester-style', get_stylesheet_uri() );

	//wp_enqueue_style( 'bondchesterblocks-style', get_template_directory_uri() . '/css/blocks.css' );

	wp_enqueue_style( 'bondchester-theme', get_template_directory_uri() . '/css/theme.css' );

	wp_enqueue_style( 'bondchester-fonts', bondchester_fonts_url() );

	wp_enqueue_script( 'bondchester-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'bondchester-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ), '4.1.3', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'bondchester_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Registered sidebar.
 */
require get_template_directory() . '/inc/widgets.php';

/**
 * Actions and Filters.
 */
require get_template_directory() . '/inc/actions-filters.php';

require get_template_directory() . '/inc/class-wp-bootstrap-navwalker.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

function creative_social_icons( $atts, $content = null ) {
	extract(shortcode_atts( array(
		'facebook' => '',
		'googleplus' => '',
		'twitter' => '',
		'pinterest' => '',
		'instagram' => '',
		'youtube' => '',
		'linkedin' => '',
		'align' => 'left'
	), $atts ) );

	ob_start();

	?>
	<ul class="ci-social-icons text-<?php echo $align ?>">
		<?php if ( ! empty($facebook) ) { ?>
			<li><a class="facebook" href="<?php echo $facebook; ?>" target="_blank"><i class="fab fa-facebook-square"></i></a></li>
		<?php } ?>
		<?php if ( ! empty($googleplus) ) { ?>
			<li><a class="googleplus" href="<?php echo $googleplus; ?>" target="_blank"><i class="fab fa-google-plus-g"></i></a></li>
		<?php } ?>
		<?php if ( ! empty($twitter) ) { ?>
			<li><a class="twitter" href="<?php echo $twitter; ?>" target="_blank"><i class="fab fa-twitter"></i></a></li>
		<?php } ?>
		<?php if ( ! empty($pinterest) ) { ?>
			<li><a class="pinterest" href="<?php echo $pinterest; ?>" target="_blank"><i class="fab fa-pinterest"></i></a></li>
		<?php } ?>
		<?php if ( ! empty($instagram) ) { ?>
			<li><a class="instagram" href="<?php echo $instagram; ?>" target="_blank"><i class="fab fa-instagram"></i></a></li>
		<?php } ?>
		<?php if ( ! empty($youtube) ) { ?>
			<li><a class="youtube" href="<?php echo $youtube; ?>" target="_blank"><i class="fab fa-youtube-square"></i></a></li>
		<?php } ?>
		<?php if ( ! empty($linkedin) ) { ?>
			<li><a class="linkedin" href="<?php echo $linkedin; ?>" target="_blank"><i class="fab fa-linkedin"></i></a></li>
		<?php } ?>
	</ul>
	<?php
	return ob_get_clean();
}
add_shortcode( 'creative-social-icons', 'creative_social_icons' );


function mamashemp_woo_cart_icon( $atts, $content = null ) {

	ob_start();

	if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
 
        $count = WC()->cart->cart_contents_count;
		?>
		<a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>">
			<span class="cart-text"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAeCAYAAABE4bxTAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQyIDc5LjE2MDkyNCwgMjAxNy8wNy8xMy0wMTowNjozOSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTggKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjM0RTAzMEQxNDk1RTExRTlBOTYzOEJEMjZCNUU2NEE0IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjM0RTAzMEQyNDk1RTExRTlBOTYzOEJEMjZCNUU2NEE0Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MzRFMDMwQ0Y0OTVFMTFFOUE5NjM4QkQyNkI1RTY0QTQiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MzRFMDMwRDA0OTVFMTFFOUE5NjM4QkQyNkI1RTY0QTQiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz70mO3vAAADX0lEQVR42ryXS0hUYRTHv9GRJPIxGiW9J0LC3g97WC0qIlq0iEhaRe3aBG2Clq2KWhURBhFkZAQt2oQYFBaUPaQsGsoeWlppmePYZJk6j/4H/hdOX3emmWmcAz/ud7/v3rn/e75zzznjKS0tqzbGHAZBcAM8lXEoFBz0+crN/xh+I+17PBDUhuNSNdcHAuAiuASiuRSUB8LW3BSwCZwDq0yOTQTtAaeAeGpIrRWAdTkXBLd2gYMYrwRrwH21Pi/XgjzxePyPCQTyfhzqeDoIasArenPcDE6JJBK0CIcWMIlTL8FXuXacQ+c7OO0mqAyHRrDa5N663bYhZMVRLi3/Lw/RSztxuAK8KpYkPeSTcn6FYsNgQG1pnFsgnp7AuTHQD2I8LwZFHI+AVvAF1HsTKG0HvWAmz6+BoxQjDzkBtnJNMvteS5CIOQuWcK6R1SDG3zgCarn2DOyQGEJgjyQS9IbZ2hHkB5/oDbEnSlAp+KDWxKrAZCXwNh7WTu8XWemkFWv9OrrdPsFRfmmOyZsuVufP+SCxqWC59RNzQQXHYb6cYyvAMo5j9J5JKoj2WGVun1XvXoAejkvoEW0zuDVi38BrtbZebe9n68WTCnrIbXJsIyjkuAO8db4MMMu6d74ad4KP3K481knHHklXkZIgXDhAUY5VM1gNPdel1uaocbHlsQf4rZgqRTq/XXfLkMlM5yMJ7AXqPKDiyM/gdra3Ul3XpsZShiaq2GpJV9AtpnTDfd+i1pqZfwy3bDrHsq3TEjx0sxWjPfYDvf8Q1ME8s4Hn22TfmejEExHOi4Dd4C6vLVCC1iJ2RpkIdfzcc+nFjGumVhlbDseY1LJp8iLbEVtN6W6Z2E2V8rNlnUy+JhNBAZaSbNod8N5twZvCzUF+/lWq0NapYutRic4euzlAiuhVbFc0U0ERVuN9Kt03M4AjDPDs/Q1KIagN808TS4LYO36B8vX8BL/YRgyTIZaMMNNGiClCOs/eRN5J1UNObSq0kqQ/xXvHKPqHIxQv2oDjGaePTlfQbHBStRPpWgELcIlVhqTlaMhEUKVVLJ3G/3KS4NUWZaewy5qvyVRQN9uECjVXD46n+lcaW7SQDV2x1cJklIeklznAXCRBeZ7taTom9x5iy9LHf8oX3C78LcAAXvgHZOrkLSoAAAAASUVORK5CYII=" alt="" />	Cart</span>
			<span class="cart-contents-count"><?php echo esc_html( $count ); ?></span>
		</a>
		<?php
    }
	
	return ob_get_clean();
}
add_shortcode( 'mamashemp-woo-cart-icon', 'mamashemp_woo_cart_icon' );